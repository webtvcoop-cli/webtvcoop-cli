/*

On veut cet URL:
1) http://webtv.coop/video/Assembl%25C3%25A9e-du-comit%25C3%25A9-ex%25C3%25A9cutif-de-la-Ville-de-Montr%25C3%25A9al%252C-26-mars-2014----Pr%25C3%25A9sentation-3%253A-Montr%25C3%25A9al-ville-intelligente/2157fe1ae611c30aa1b789b6337fbe54

On va lire cet URL:
2) http://webtv.coop/js/embed.js.php?key=2157fe1ae611c30aa1b789b6337fbe54

On trouvera la valeur de key=... a la fin du premier URL (1), apres le /.

En lisant (2) on grep pour obtenir le mediaid qui est utilise pour construire le 3e URL:
3) http://webtv.coop/media/flashcomm?action=getmediainfo&context=normal&mediaid=3942

En lisant (3) comme un XML, on obtient l'URL du FLV </mediainfo/active_media/media/file>, le titre, la description, etc.
4) http://webtv.coop/images/media/2157fe1ae611c30aa1b789b6337fbe54_1395851288.flv

*/

var req, the_key, percent, no_download = false, output_filename = false,
    http = require('http'), fs = require('fs'), mod = require('./counter.js');

process.argv.forEach(function(val, index, array) {
  var x;
  if (2 === index) {
    x = val.split("/");
    the_key = x.pop();
  } else if (index >= 3) {
    if ('--no-download' === val) {
        no_download = true;
    } else if (0 === val.indexOf('--output=')) {
        output_filename = val.split('=')[1];
    } else {
        percent = val;
    }
  }
});

req = http.get('http://webtv.coop/js/embed.js.php?key=' + the_key, function(res) {
    var prev = '', re=/&mediaid=(\d+)&/;

    res.on('data', cb);

    function doit(mediaid) {
        // get XML file:
        var req2, xml_url = 'http://webtv.coop/media/flashcomm?action=getmediainfo&context=normal&mediaid=' + mediaid;

        req2 = http.get(xml_url, function(res2) {
            var prev2 = '', re2=/<file>(.+)<\/file>/, re3=/<duration_sec>(.+)<\/duration_sec>/, the_flv_url, the_duration;

            // only interested in flv file URL
            res2.on('data', cb2);
            res2.on('end', function() {
                console.log("DONE");

                doit2(the_flv_url, the_duration);

                function doit2(flv_url, duration) {
                    var outfile, x2;
                   
                    if (!no_download) {
                        if (!output_filename) {
                            x2 = flv_url.split("/");
                            output_filename = x2.pop();
                        }
                        outfile = fs.createWriteStream(output_filename);

                        http.get(flv_url, function (res) {
                            console.log("piping...");
                            // # of bytes (avg 74k/s)
                            // show progress (console.log) every 5%
                            res.pipe(mod(duration*74264, percent||5)).pipe(outfile);
                        });
                    }
                    console.log('FLV: ' + flv_url);
                    console.log('duration: ' + duration);
                }
            });

            function cb2(chunk) {
                var d2, d3;

                if (!the_flv_url) {
                    d2 = re2.exec(chunk);

                    if (d2 && d2[1] && !d2[2]) {
                        the_flv_url = d2[1];
                    } else if (prev2) {
                        d2 = re2.exec(prev2 + chunk);
                        if (d2 && d2[1] && !d2[2]) {
                            the_flv_url = d2[1];
                        }
                    }
                }

                if (!the_duration) {
                    d3 = re3.exec(chunk);

                    if (d3 && d3[1] && !d3[2]) {
                        the_duration = d3[1];
                    } else if (prev2) {
                        d3 = re3.exec(prev2 + chunk);
                        if (d3 && d3[1] && !d3[2]) {
                            the_duration = d3[1];
                        }
                    }
                }

                if (the_duration && the_flv_url) {
                    req2.abort();
                }

                prev2 = chunk;
            }
        });
    }

    function cb(chunk) {
       var d = re.exec(chunk);
       if (d && d[1] && !d[2]) {
            req.abort();
            doit(d[1]);
       } else if (prev) {
           d = re.exec(prev + chunk);
           if (d && d[1] && !d[2]) {
                req.abort();
                doit(d[1]);
           }
       }
       prev = chunk;
    }
});

