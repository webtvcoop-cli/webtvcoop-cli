Usage
=====
*The code is terribly ugly. It works for me. Improvements welcome.*

*Vous voulez soumettre des instructions en francais? Please!*

nodejs index.js URL [--no-download] [--output=FILENAME.FLV] [10]

By default, the output filename is extracted from the flv source URL. If you don't want to download the file, pass the --no-download option. Finally, a decimal number passed as an argument changes how often is progress shown (every 10% downloaded, every 1%, 5%, 0.1%, etc.)

Pass it as a URL any video page from http://webtv.coop/ such as the following:
http://webtv.coop/video/Le-d%25C3%25A9bat-sur-les-enjeux-d%25E2%2580%2599environnement%252C-de-transport-et-d%25E2%2580%2599am%25C3%25A9nagement-propres-%25C3%25A0-la-m%25C3%25A9tropole/39d4b761ed05efbd934c24b07a202448

What really matters is actually the string after the last / in that URL. It's used to get an XML file with more information about that video. In that information, we find the URL of the actual FLV video file, which we download unless --no-download is passed as an argument.

Other
-----
avconv -i outputfile.flv transcoded.ogv # outputs theora (ogg) video

If we upload the FLV (Flash video) to http://archive.org/ it should be transcoded to a few formats automatically according to http://archive.org/help/derivatives.php

