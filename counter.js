var Stream = require('stream').Stream;

module.exports = function(tot, per) {
  var stream = new Stream(), cnt = 0, prev = 0, stp = tot * per / 100;

  stream.writable = true;
  stream.readable = true;

  stream.write = function(data) {
    cnt += data.length;
    if ( Math.round(cnt/stp-.5) !== Math.round(prev/stp-.5)  ) {
        console.log(Math.round(cnt*100/tot) + '%');
    }
    prev = cnt;
    stream.emit("data", data);
  }

  stream.end = function() {
    console.log("end")
    stream.emit("end")
  }

  stream.destroy = function() {
    console.log("destroy")
    stream.emit("close")
  }

  return stream;
}

